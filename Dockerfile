FROM nginx:1.15

ENV ROOT_PATH /usr/share/nginx/html

# Include our nginx conf
COPY config/default.conf /default.conf

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

RUN echo "ServerName localhost" >> /etc/nginx/conf.d/default.conf \
  && echo "Include conf/extra/vhost.conf" >> /etc/nginx/conf.d/default.conf

# Change entrypoint
COPY config/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
